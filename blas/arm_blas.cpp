/// @file arm_blas.cpp
///


#include "arm_compute/core/NEON/kernels/assembly/arm_gemm.hpp"
#include "arm_compute/runtime/CPUUtils.h"

#include "arm_blas.h"

#include <algorithm>
#include <cassert>
#include <cstring>
#include <mutex>

/// Convert CBLAS enum for matrix transpose to arm_gemm convention.
static inline bool is_cblas_transpose(CBLAS_TRANSPOSE transpose) {
  return transpose == CblasNoTrans ? false : true;
}

/// @return CPU information.
/// @todo: it would be better to put the initialization code in a separate init routine
static const arm_compute::CPUInfo& get_ci() {
    static arm_compute::CPUInfo cpuInfo;
    static bool cpuInfoInitialized = false;
    if (!cpuInfoInitialized) {
        static std::mutex cpuInfoInitMutex;
        std::lock_guard<std::mutex> lck(cpuInfoInitMutex);
        if (!cpuInfoInitialized)  // Check again to be sure not to initialize twice
            arm_compute::get_cpu_configuration(cpuInfo);
        cpuInfoInitialized = true;
    }
    return cpuInfo;
}

void arm_cblas_init() {
  get_ci();
}

template<typename IN_TYPE, typename OUT_TYPE>
void cblas_gemm_armcl(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                 CBLAS_TRANSPOSE TransB, int M, int N, const int K,
                 const OUT_TYPE alpha, const IN_TYPE *A, int lda,
                 const IN_TYPE *B, int ldb, OUT_TYPE beta, OUT_TYPE *C,
                 int ldc) {
  if (layout == CblasColMajor) {
    // arm-gemm library does not support CblasColMajor layout
    // A column major matrix is the transpose of the equivalent row major
    // representation
    // When using the row-major version of GEMM, we need to swap the order of
    // matrices
    // and the order of the parameters M, N and lda, ldb.
    // For more details, see
    // https://www.christophlassner.de/using-blas-from-c-with-row-major-data.html
    std::swap(A, B);
    std::swap(M, N);
    std::swap(lda, ldb);
  }

  if (beta == static_cast<OUT_TYPE>(0)) {
    // If BETA is zero we first fill the accumulator matrix, C, with zeroes
    // since we assume that the intent of the zero is that any data in C be
    // overwritten. If we don't fill with zeroes then there is a chance that
    // NaNs within C leak through the GEMM (since 0*NaN + x = NaN).
    std::memset(C, 0x00, M*ldc*sizeof(OUT_TYPE));
  }

  bool trA = is_cblas_transpose(TransA);
  bool trB = is_cblas_transpose(TransB);
  const int max_threads = 1;

  auto gemm_operation = arm_gemm::gemm<IN_TYPE, OUT_TYPE>(get_ci(), M, N, K, 1, 1, trA, trB, alpha, beta, max_threads, false);
  size_t workingSize = gemm_operation->get_working_size();
  uint8_t* tmpMemory = new uint8_t[workingSize];
  gemm_operation->set_working_space(tmpMemory);
  gemm_operation->set_arrays(A, lda, 0, 0, B, ldb, 0, C, ldc, 0, 0);
  auto windowSize = gemm_operation->get_window_size();
  gemm_operation->execute(0, windowSize, 0);

  delete[] tmpMemory;
}

void cblas_sgemm(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                 CBLAS_TRANSPOSE TransB, int M, int N, int K,
                 const float alpha, const float *A, int lda,
                 const float *B, int ldb, float beta, float *C,
                 int ldc) {
    cblas_gemm_armcl<float, float>(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
}

#ifdef __ARM_FP16_ARGS
void cblas_hgemm(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                 CBLAS_TRANSPOSE TransB, int M, int N, int K,
                 __fp16 alpha, const __fp16 *A, int lda,
                 const __fp16 *B, int ldb, const __fp16 beta, __fp16 *C,
                 int ldc) {
    cblas_gemm_armcl<__fp16, __fp16>(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
}
#endif

void cblas_gemm_s16(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                    CBLAS_TRANSPOSE TransB, int M, int N,
                    int K, int32_t alpha, const int16_t *A,
                    int lda, const int16_t *B, int ldb,
                    int32_t beta, int32_t *C, int ldc) {
#ifdef __aarch64__
    cblas_gemm_armcl<int16_t, int32_t>(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
#else
    printf("int16_t gemm is only supported for 64-bit architectures\n");
    assert(false);
#endif
}

void cblas_gemm_s8(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                   CBLAS_TRANSPOSE TransB, int M, int N,
                   int K, int32_t alpha, const int8_t *A,
                   int lda, const int8_t *B, int ldb,
                   int32_t beta, int32_t *C, int ldc) {
#ifdef __aarch64__
    cblas_gemm_armcl<int8_t, int32_t>(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
#else
    printf("int8_t gemm is only supported for 64-bit architectures\n");
    assert(false);
#endif
}
