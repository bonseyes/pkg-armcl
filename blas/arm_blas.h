// @file arm_gemm.h
//
//  \date Created on: Jan 1, 2018
//  \author Gopalakrishna Hegde
//
//   Description:
//
//
//

#pragma once

#include <openblas/cblas.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// We need support from another blas library to implement missing functions
// So comment this out to avoid collisions
#if 0
typedef enum { CblasRowMajor = 101, CblasColMajor = 102 } CBLAS_LAYOUT;
typedef enum CBLAS_ORDER CBLAS_LAYOUT;

typedef enum {
  CblasNoTrans = 111,
  CblasTrans = 112,
  CblasConjTrans = 113
} CBLAS_TRANSPOSE;

#endif


/// Intialize the ARM cblas library
/// If not called the initialization will be performed at the first cblas call.
void arm_cblas_init();


void cblas_sgemm(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                 CBLAS_TRANSPOSE TransB, int M, int N, int K,
                 float alpha, const float *A, int lda,
                 const float *B, int ldb, float beta, float *C,
                 int ldc);

#ifdef __ARM_FP16_ARGS
void cblas_hgemm(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                 CBLAS_TRANSPOSE TransB, int M, int N, int K,
                 __fp16 alpha, const __fp16 *A, int lda,
                 const __fp16 *B, int ldb, __fp16 beta, __fp16 *C,
                 int ldc);
#endif

void cblas_gemm_s16(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                    CBLAS_TRANSPOSE TransB, int M, int N,
                    int K, int32_t alpha, const int16_t *A,
                    int lda, const int16_t *B, int ldb,
                    int32_t beta, int32_t *C, int ldc);

void cblas_gemm_s8(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                   CBLAS_TRANSPOSE TransB, int M, int N,
                   int K, int32_t alpha, const int8_t *A,
                   int lda, const int8_t *B, int ldb,
                   int32_t beta, int32_t *C, int ldc);

#ifdef __cplusplus
}
#endif
