/*
 * Copyright (c) 2018 ARM Limited.
 */

#include "Permute.hpp"

#include <arm_neon.h>
#include <cstdint>

namespace arm_permute {

// 4-byte NCHW to NHWC specialisation
template <>
void nchw_to_nhwc<4>(const void* const inptr, void* const outptr,
                     const unsigned int n_batches,
                     const unsigned int n_channels, const unsigned int n_rows,
                     const unsigned int n_cols, unsigned int in_batch_stride,
                     unsigned int in_channel_stride, unsigned int in_row_stride,
                     unsigned int out_batch_stride, unsigned int out_row_stride,
                     unsigned int out_col_stride) {
  typedef int32_t T;
  const T* const in = static_cast<const T* const>(inptr);
  T* const out = static_cast<T* const>(outptr);

  // Fill in the stride values
  in_row_stride = (in_row_stride) ? in_row_stride : n_cols;
  in_channel_stride =
      (in_channel_stride) ? in_channel_stride : n_rows * in_row_stride;
  in_batch_stride =
      (in_batch_stride) ? in_batch_stride : n_channels * in_channel_stride;

  out_col_stride = (out_col_stride) ? out_col_stride : n_channels;
  out_row_stride = (out_row_stride) ? out_row_stride : n_cols * out_col_stride;
  out_batch_stride =
      (out_batch_stride) ? out_batch_stride : n_rows * out_row_stride;

  // Perform the re-ordering
  for (unsigned int n = 0; n < n_batches; n++) {
    const T* const in_batch = in + n * in_batch_stride;
    T* const out_batch = out + n * out_batch_stride;

    for (unsigned int i = 0; i < n_rows; i++) {
      const T* const in_row = in_batch + i * in_row_stride;
      T* const out_row = out_batch + i * out_row_stride;

      unsigned int j = 0, j_remaining = n_cols;
      for (; j_remaining >= 4; j += 4, j_remaining -= 4) {
        unsigned int c = 0, c_remaining = n_channels;
        for (; c_remaining >= 4; c += 4, c_remaining -= 4) {
          // Read 4 channels worth of 4 columns, then zip to produce 4 columns
          // worth of 4 channels.
          int32x4_t channel_pixels[4];
          channel_pixels[0] =
              vld1q_s32(in_row + (c + 0) * in_channel_stride + j);
          channel_pixels[1] =
              vld1q_s32(in_row + (c + 1) * in_channel_stride + j);
          channel_pixels[2] =
              vld1q_s32(in_row + (c + 2) * in_channel_stride + j);
          channel_pixels[3] =
              vld1q_s32(in_row + (c + 3) * in_channel_stride + j);

          const auto zip1 = vzipq_s32(channel_pixels[0], channel_pixels[2]);
          const auto zip2 = vzipq_s32(channel_pixels[1], channel_pixels[3]);
          const auto out_0 = vzipq_s32(zip1.val[0], zip2.val[0]);
          const auto out_1 = vzipq_s32(zip1.val[1], zip2.val[1]);

          vst1q_s32(out_row + (j + 0) * out_col_stride + c, out_0.val[0]);
          vst1q_s32(out_row + (j + 1) * out_col_stride + c, out_0.val[1]);
          vst1q_s32(out_row + (j + 2) * out_col_stride + c, out_1.val[0]);
          vst1q_s32(out_row + (j + 3) * out_col_stride + c, out_1.val[1]);
        }
        for (; c_remaining; c++, c_remaining--) {
          for (unsigned int _j = 0; _j < 4; _j++) {
            const T* const in_col = in_row + j + _j;
            T* const out_col = out_row + (j + _j) * out_col_stride;
            const T* const in_channel = in_col + c * in_channel_stride;
            out_col[c] = *(in_channel);
          }
        }
      }
      for (; j_remaining >= 2; j += 2, j_remaining -= 2) {
        unsigned int c = 0, c_remaining = n_channels;
        for (; c_remaining >= 2; c += 2, c_remaining -= 2) {
          // Read 2 channels worth of 2 columns, then zip to produce 2 columns
          // worth of 2 channels.
          int32x2_t channel_pixels[2];
          channel_pixels[0] =
              vld1_s32(in_row + (c + 0) * in_channel_stride + j);
          channel_pixels[1] =
              vld1_s32(in_row + (c + 1) * in_channel_stride + j);

          const auto output = vzip_s32(channel_pixels[0], channel_pixels[1]);

          vst1_s32(out_row + (j + 0) * out_col_stride + c, output.val[0]);
          vst1_s32(out_row + (j + 1) * out_col_stride + c, output.val[1]);
        }
        for (; c_remaining; c++, c_remaining--) {
          for (unsigned int _j = 0; _j < 2; _j++) {
            const T* const in_col = in_row + j + _j;
            T* const out_col = out_row + (j + _j) * out_col_stride;
            const T* const in_channel = in_col + c * in_channel_stride;
            out_col[c] = *(in_channel);
          }
        }
      }
      for (; j_remaining; j++, j_remaining--) {
        const T* const in_col = in_row + j;
        T* const out_col = out_row + j * out_col_stride;

        for (unsigned int c = 0; c < n_channels; c++) {
          const T* const in_channel = in_col + c * in_channel_stride;
          out_col[c] = *(in_channel);
        }
      }
    }
  }
}

// 4-byte NHWC to NCHW implementation
template <>
void nhwc_to_nchw<4>(const void* const inptr, void* const outptr,
                     const unsigned int n_batches, const unsigned int n_rows,
                     const unsigned int n_cols, const unsigned int n_channels,
                     unsigned int in_batch_stride, unsigned int in_row_stride,
                     unsigned int in_col_stride, unsigned int out_batch_stride,
                     unsigned int out_channel_stride,
                     unsigned int out_row_stride) {
  typedef int32_t T;
  const T* const in = static_cast<const T* const>(inptr);
  T* const out = static_cast<T* const>(outptr);

  // Fill in stride values
  in_col_stride = (in_col_stride) ? in_col_stride : n_channels;
  in_row_stride = (in_row_stride) ? in_row_stride : n_cols * in_col_stride;
  in_batch_stride =
      (in_batch_stride) ? in_batch_stride : n_rows * in_row_stride;

  out_row_stride = (out_row_stride) ? out_row_stride : n_cols;
  out_channel_stride =
      (out_channel_stride) ? out_channel_stride : n_rows * out_row_stride;
  out_batch_stride =
      (out_batch_stride) ? out_batch_stride : n_channels * out_channel_stride;

  // Perform the re-ordering
  // For every batch
  for (unsigned int n = 0; n < n_batches; n++) {
    const T* const in_batch = in + n * in_batch_stride;
    T* const out_batch = out + n * out_batch_stride;

    // For every row
    for (unsigned int i = 0; i < n_rows; i++) {
      const T* const in_i = in_batch + i * in_row_stride;
      T* const out_i = out_batch + i * out_row_stride;

      // For every column, beginning with chunks of 4
      unsigned int j = 0, j_remaining = n_cols;
      for (; j_remaining >= 4; j += 4, j_remaining -= 4) {
        // For every channel, beginning with chunks of 4
        unsigned int c = 0, c_remaining = n_channels;
        for (; c_remaining >= 4; c += 4, c_remaining -= 4) {
          // Read 4 columns worth of 4 channels then zip to produce 4 channels
          // worth of 4 columns.
          int32x4_t pixel_channels[4];
          pixel_channels[0] = vld1q_s32(in_i + (j + 0) * in_col_stride + c);
          pixel_channels[1] = vld1q_s32(in_i + (j + 1) * in_col_stride + c);
          pixel_channels[2] = vld1q_s32(in_i + (j + 2) * in_col_stride + c);
          pixel_channels[3] = vld1q_s32(in_i + (j + 3) * in_col_stride + c);

          const auto zip1 = vzipq_s32(pixel_channels[0], pixel_channels[2]);
          const auto zip2 = vzipq_s32(pixel_channels[1], pixel_channels[3]);
          const auto out_0 = vzipq_s32(zip1.val[0], zip2.val[0]);
          const auto out_1 = vzipq_s32(zip1.val[1], zip2.val[1]);

          vst1q_s32(out_i + j + (c + 0) * out_channel_stride, out_0.val[0]);
          vst1q_s32(out_i + j + (c + 1) * out_channel_stride, out_0.val[1]);
          vst1q_s32(out_i + j + (c + 2) * out_channel_stride, out_1.val[0]);
          vst1q_s32(out_i + j + (c + 3) * out_channel_stride, out_1.val[1]);
        }
        for (; c_remaining; c++, c_remaining--) {
          for (unsigned int _j = 0; _j < 4; _j++) {
            const T* const in_j = in_i + (j + _j) * in_col_stride;
            T* const out_j = out_i + (j + _j);

            const T* const in_channel = in_j + c;
            T* const out_channel = out_j + c * out_channel_stride;
            *(out_channel) = *(in_channel);
          }
        }
      }
      for (; j_remaining >= 2; j += 2, j_remaining -= 2) {
        unsigned int c = 0, c_remaining = n_channels;
        for (; c_remaining >= 2; c += 2, c_remaining -= 2) {
          // Read 2 columns worth of 2 channels then zip to produce 2 channels
          // worth of 2 columns.
          int32x2_t pixel_channels[2];
          pixel_channels[0] = vld1_s32(in_i + (j + 0) * in_col_stride + c);
          pixel_channels[1] = vld1_s32(in_i + (j + 1) * in_col_stride + c);

          const auto output = vzip_s32(pixel_channels[0], pixel_channels[1]);

          vst1_s32(out_i + j + (c + 0) * out_channel_stride, output.val[0]);
          vst1_s32(out_i + j + (c + 1) * out_channel_stride, output.val[1]);
        }
        for (; c_remaining; c++, c_remaining--) {
          for (unsigned int _j = 0; _j < 2; _j++) {
            const T* const in_j = in_i + (j + _j) * in_col_stride;
            T* const out_j = out_i + (j + _j);

            const T* const in_channel = in_j + c;
            T* const out_channel = out_j + c * out_channel_stride;
            *(out_channel) = *(in_channel);
          }
        }
      }
      for (; j_remaining; j++, j_remaining--) {
        const T* const in_j = in_i + j * in_col_stride;
        T* const out_j = out_i + j;

        // For every channel
        for (unsigned int c = 0; c < n_channels; c++) {
          const T* const in_channel = in_j + c;
          T* const out_channel = out_j + c * out_channel_stride;
          *(out_channel) = *(in_channel);
        }
      }
    }
  }
}

}  // namespace arm_permute
