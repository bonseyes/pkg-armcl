/*
 * Copyright (c) 2018 ARM Limited.
 */
#pragma once

#if !(defined(__arm__) || defined(__aarch64__))
#error "Only supported on ARM platforms"
#endif

namespace arm_utils {

template <typename T>
void AddBias(
  const T *const input, const T *const bias,
  int batch_size, int out_ch, int out_h, int out_w,
  T *const output
);

}  // arm_utils
