/*
 * Copyright (c) 2018 ARM Limited.
 */
#pragma once

#if !(defined(__arm__) || defined(__aarch64__))
#error "Only supported on ARM platforms"
#endif

#include <cstddef>

namespace arm_permute {

/** Re-order a tensor from NCHW format to NHWC.
 *
 * @note The stride parameters are optional and are provided to allow padding in
 * either input or output tensors.
 *
 * @param[in] in Input tensor in NCHW format.
 * @param[out] out Output tensor, to be written in NHWC format.
 * @param n_batches Number of batches in the tensors.
 * @param n_channels Number of channels in the tensors
 * @param n_rows Height of the tensor
 * @param n_cols Width of the tensor
 * @param in_batch_stride Stride over batches in the input tensor. If `0`
 * defaults to `n_channels * in_channel_stride`.
 * @param in_channel_stride Stride over channels in the input tensor. If `0`
 * defaults to `n_rows * in_row_stride`.
 * @param in_row_stride Stride over rows in the input tensor. If `0` defaults to
 * `n_cols`.
 * @param out_batch_stride Stride over batches in the output tensor. If `0`
 * defaults to `n_rows * out_row_stride`.
 * @param out_row_stride Stride over rows in the output tensor. If `0` defaults
 * to `n_cols * out_col_stride`.
 * @param out_col_stride Stride over columns in the output tensor. If `0`
 * defaults to `n_channels`.
 */
template <size_t n_bytes_per_element>
void nchw_to_nhwc(const void* const in, void* const out,
                  const unsigned int n_batches, const unsigned int n_channels,
                  const unsigned int n_rows, const unsigned int n_cols,
                  const unsigned int in_batch_stride = 0,
                  const unsigned int in_channel_stride = 0,
                  const unsigned int in_row_stride = 0,
                  const unsigned int out_batch_stride = 0,
                  const unsigned int out_row_stride = 0,
                  const unsigned int out_col_stride = 0);

/** Re-order a tensor from NHWC format to NCHW.
 *
 * @note The stride parameters are optional and are provided to allow padding in
 * either input or output tensors.
 *
 * @param[in] in Input tensor in NHWC format.
 * @param[out] out Output tensor, to be written in NCHW format.
 * @param n_batches Number of batches in the tensors.
 * @param n_rows Height of the tensor
 * @param n_cols Width of the tensor
 * @param n_channels Number of channels in the tensors
 * @param in_batch_stride Stride over batches in the input tensor. If `0`
 * defaults to `n_rows * in_row_stride`.
 * @param in_row_stride Stride over rows in the input tensor. If `0` defaults to
 * `n_cols * in_col_stride`.
 * @param in_col_stride Stride over columns in the input tensor. If `0` defaults
 * to `n_channels`.
 * @param out_batch_stride Stride over batches in the output tensor. If `0`
 * defaults to `n_channels * out_channel_stride`.
 * @param out_channel_stride Stride over channels in the output tensor. If `0`
 * defaults to `n_rows * out_row_stride`.
 * @param out_row_stride Stride over rows in the output tensor. If `0` defaults
 * to `n_cols`.
 */
template <size_t n_bytes_per_element>
void nhwc_to_nchw(const void* const in, void* const out,
                  const unsigned int n_batches, const unsigned int n_rows,
                  const unsigned int n_cols, const unsigned int n_channels,
                  const unsigned int in_batch_stride = 0,
                  const unsigned int in_row_stride = 0,
                  const unsigned int in_col_stride = 0,
                  const unsigned int out_batch_stride = 0,
                  const unsigned int out_channel_stride = 0,
                  const unsigned int out_row_stride = 0);

}  // namespace arm_permute
