/*
 * Copyright (c) 2018 ARM Limited.
 */

#include "AddBias.hpp"

#include <arm_neon.h>
#include <cstdint>

namespace arm_utils {

template <>
void AddBias<float>(
  const float *const input, const float *const bias,
  int batch_size, int out_ch, int out_h, int out_w,
  float *const output
) {
  const int col_stride = out_ch;
  const int row_stride = out_w * col_stride;
  const int batch_stride = out_h * row_stride;

  int c_remaining = out_ch;
  int c = 0;

  for (; c_remaining >= 16; c += 16, c_remaining -= 16) {
    // Work on 16 channels at a time
    register const float32x4_t vbias_a = vld1q_f32(bias + c);
    register const float32x4_t vbias_b = vld1q_f32(bias + c + 4);
    register const float32x4_t vbias_c = vld1q_f32(bias + c + 8);
    register const float32x4_t vbias_d = vld1q_f32(bias + c + 12);
    const float *const in_cptr = input + c;
    float *const out_cptr = output + c;

    for (int b = 0; b < batch_size; b++) {
      const float *const in_bptr = in_cptr + b * batch_stride;
      float *const out_bptr = out_cptr + b * batch_stride;
      for (int i = 0; i < out_h; i++) {
        const float *const in_iptr = in_bptr + i * row_stride;
        float *const out_iptr = out_bptr + i * row_stride;
        for (int j = 0; j < out_w; j++) {
          const float *const in_jptr = in_iptr + j * col_stride;
          float *const out_jptr = out_iptr + j * col_stride;
          register const float32x4_t vinput_a = vld1q_f32(in_jptr);
          register const float32x4_t vinput_b = vld1q_f32(in_jptr + 4);
          register const float32x4_t vinput_c = vld1q_f32(in_jptr + 8);
          register const float32x4_t vinput_d = vld1q_f32(in_jptr + 12);
          register const float32x4_t voutput_a = vaddq_f32(vinput_a, vbias_a);
          register const float32x4_t voutput_b = vaddq_f32(vinput_b, vbias_b);
          register const float32x4_t voutput_c = vaddq_f32(vinput_c, vbias_c);
          register const float32x4_t voutput_d = vaddq_f32(vinput_d, vbias_d);
          vst1q_f32(out_jptr, voutput_a);
          vst1q_f32(out_jptr + 4, voutput_b);
          vst1q_f32(out_jptr + 8, voutput_c);
          vst1q_f32(out_jptr + 12, voutput_d);
        }
      }
    }
  }
  for (; c_remaining >= 4; c += 4, c_remaining -= 4) {
    // Work on 4 channels at a time
    const float32x4_t vbias = vld1q_f32(bias + c);
    const float *const in_cptr = input + c;
    float *const out_cptr = output + c;

    for (int b = 0; b < batch_size; b++) {
      const float *const in_bptr = in_cptr + b * batch_stride;
      float *const out_bptr = out_cptr + b * batch_stride;
      for (int i = 0; i < out_h; i++) {
        const float *const in_iptr = in_bptr + i * row_stride;
        float *const out_iptr = out_bptr + i * row_stride;
        for (int j = 0; j < out_w; j++) {
          const float *const in_jptr = in_iptr + j * col_stride;
          float *const out_jptr = out_iptr + j * col_stride;
          const float32x4_t vinput = vld1q_f32(in_jptr);
          const float32x4_t voutput = vaddq_f32(vinput, vbias);
          vst1q_f32(out_jptr, voutput);
        }
      }
    }
  }
  for (; c_remaining >= 2; c += 2, c_remaining -= 2) {
    // Work on 2 channels at a time
    const float32x2_t vbias = vld1_f32(bias + c);
    const float *const in_cptr = input + c;
    float *const out_cptr = output + c;

    for (int b = 0; b < batch_size; b++) {
      const float *const in_bptr = in_cptr + b * batch_stride;
      float *const out_bptr = out_cptr + b * batch_stride;
      for (int i = 0; i < out_h; i++) {
        const float *const in_iptr = in_bptr + i * row_stride;
        float *const out_iptr = out_bptr + i * row_stride;
        for (int j = 0; j < out_w; j++) {
          const float *const in_jptr = in_iptr + j * col_stride;
          float *const out_jptr = out_iptr + j * col_stride;
          const float32x2_t vinput = vld1_f32(in_jptr);
          const float32x2_t voutput = vadd_f32(vinput, vbias);
          vst1_f32(out_jptr, voutput);
        }
      }
    }
  }

  for (; c_remaining; c++, c_remaining--) {
    // Work on a single channel at a time
    const float vbias = bias[c];
    const float *const in_cptr = input + c;
    float *const out_cptr = output + c;

    for (int b = 0; b < batch_size; b++) {
      const float *const in_bptr = in_cptr + b * batch_stride;
      float *const out_bptr = out_cptr + b * batch_stride;
      for (int i = 0; i < out_h; i++) {
        const float *const in_iptr = in_bptr + i * row_stride;
        float *const out_iptr = out_bptr + i * row_stride;
        for (int j = 0; j < out_w; j++) {
          const float *const in_jptr = in_iptr + j * col_stride;
          float *const out_jptr = out_iptr + j * col_stride;
          const float vinput = *(in_jptr);
          const float voutput = vinput + vbias;
          *(out_jptr) = voutput;
        }
      }
    }
  }
}

}  // arm_utils
